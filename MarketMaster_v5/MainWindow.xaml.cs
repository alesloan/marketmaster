﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WinInterop = System.Windows.Interop;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace MarketMaster_v5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            winMain.SourceInitialized += new EventHandler(win_SourceInitialized);
            LoadStocksAsync();
            LoadTweetsAsync();
            LoadNewsAsync();
            LoadStocksHaltsAsync();
        }

        #region Windows Controls
        private void MinimizeButton_Click(object sender, RoutedEventArgs ex)
        {

            this.WindowState = WindowState.Minimized;

        }

        private void MaximizeButton_Click(object sender, RoutedEventArgs ex)
        {
            if (this.WindowState == WindowState.Maximized)
            {
                this.WindowState = WindowState.Normal;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs ex)
        {
            this.Close();
        }


        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            ButtonOpenMenu.Visibility = Visibility.Visible;
        }


        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //UserControl usc = null;
            //GridMain.Children.Clear();

            //switch (((ListViewItem)((ListView)sender).SelectedItem).Name)
            //{
            //    case "ItemHome":
            //        usc = new UserControlHome();
            //        GridMain.Children.Add(usc);
            //        break;
            //    case "ItemCreate":
            //        usc = new UserControlCreate();
            //        GridMain.Children.Add(usc);
            //        break;
            //    default:
            //        break;
            //}
        }


        #region Avoid hiding task bar upon maximalisation

        private static System.IntPtr WindowProc(
              System.IntPtr hwnd,
              int msg,
              System.IntPtr wParam,
              System.IntPtr lParam,
              ref bool handled)
        {
            switch (msg)
            {
                case 0x0024:
                    WmGetMinMaxInfo(hwnd, lParam);
                    handled = false;
                    break;
            }

            return (System.IntPtr)0;
        }

        void win_SourceInitialized(object sender, EventArgs e)
        {
            System.IntPtr handle = (new WinInterop.WindowInteropHelper(this)).Handle;
            WinInterop.HwndSource.FromHwnd(handle).AddHook(new WinInterop.HwndSourceHook(WindowProc));
        }

        private static void WmGetMinMaxInfo(System.IntPtr hwnd, System.IntPtr lParam)
        {

            MINMAXINFO mmi = (MINMAXINFO)Marshal.PtrToStructure(lParam, typeof(MINMAXINFO));

            // Adjust the maximized size and position to fit the work area of the correct monitor
            int MONITOR_DEFAULTTONEAREST = 0x00000002;
            System.IntPtr monitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);

            if (monitor != System.IntPtr.Zero)
            {

                MONITORINFO monitorInfo = new MONITORINFO();
                GetMonitorInfo(monitor, monitorInfo);
                RECT rcWorkArea = monitorInfo.rcWork;
                RECT rcMonitorArea = monitorInfo.rcMonitor;
                mmi.ptMaxPosition.x = Math.Abs(rcWorkArea.left - rcMonitorArea.left);
                mmi.ptMaxPosition.y = Math.Abs(rcWorkArea.top - rcMonitorArea.top);
                mmi.ptMaxSize.x = Math.Abs(rcWorkArea.right - rcWorkArea.left);
                mmi.ptMaxSize.y = Math.Abs(rcWorkArea.bottom - rcWorkArea.top);
                mmi.ptMaxTrackSize.x = Math.Abs(rcWorkArea.Width);
                mmi.ptMaxTrackSize.y = Math.Abs(rcWorkArea.Height);
            }

            Marshal.StructureToPtr(mmi, lParam, true);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            /// <summary>
            /// x coordinate of point.
            /// </summary>
            public int x;
            /// <summary>
            /// y coordinate of point.
            /// </summary>
            public int y;

            /// <summary>
            /// Construct a point of coordinates (x,y).
            /// </summary>
            public POINT(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MINMAXINFO
        {
            public POINT ptReserved;
            public POINT ptMaxSize;
            public POINT ptMaxPosition;
            public POINT ptMinTrackSize;
            public POINT ptMaxTrackSize;
        };

        void win_Loaded(object sender, RoutedEventArgs e)
        {
            winMain.WindowState = WindowState.Maximized;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class MONITORINFO
        {
            /// <summary>
            /// </summary>            
            public int cbSize = Marshal.SizeOf(typeof(MONITORINFO));

            /// <summary>
            /// </summary>            
            public RECT rcMonitor = new RECT();

            /// <summary>
            /// </summary>            
            public RECT rcWork = new RECT();

            /// <summary>
            /// </summary>            
            public int dwFlags = 0;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 0)]
        public struct RECT
        {
            /// <summary> Win32 </summary>
            public int left;
            /// <summary> Win32 </summary>
            public int top;
            /// <summary> Win32 </summary>
            public int right;
            /// <summary> Win32 </summary>
            public int bottom;

            /// <summary> Win32 </summary>
            public static readonly RECT Empty = new RECT();

            /// <summary> Win32 </summary>
            public int Width
            {
                get { return Math.Abs(right - left); }  // Abs needed for BIDI OS
            }
            /// <summary> Win32 </summary>
            public int Height
            {
                get { return bottom - top; }
            }

            /// <summary> Win32 </summary>
            public RECT(int left, int top, int right, int bottom)
            {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }


            /// <summary> Win32 </summary>
            public RECT(RECT rcSrc)
            {
                this.left = rcSrc.left;
                this.top = rcSrc.top;
                this.right = rcSrc.right;
                this.bottom = rcSrc.bottom;
            }

            /// <summary> Win32 </summary>
            public bool IsEmpty
            {
                get
                {
                    // BUGBUG : On Bidi OS (hebrew arabic) left > right
                    return left >= right || top >= bottom;
                }
            }
            /// <summary> Return a user friendly representation of this struct </summary>
            public override string ToString()
            {
                if (this == RECT.Empty) { return "RECT {Empty}"; }
                return "RECT { left : " + left + " / top : " + top + " / right : " + right + " / bottom : " + bottom + " }";
            }

            /// <summary> Determine if 2 RECT are equal (deep compare) </summary>
            public override bool Equals(object obj)
            {
                if (!(obj is Rect)) { return false; }
                return (this == (RECT)obj);
            }

            /// <summary>Return the HashCode for this struct (not garanteed to be unique)</summary>
            public override int GetHashCode()
            {
                return left.GetHashCode() + top.GetHashCode() + right.GetHashCode() + bottom.GetHashCode();
            }


            /// <summary> Determine if 2 RECT are equal (deep compare)</summary>
            public static bool operator ==(RECT rect1, RECT rect2)
            {
                return (rect1.left == rect2.left && rect1.top == rect2.top && rect1.right == rect2.right && rect1.bottom == rect2.bottom);
            }

            /// <summary> Determine if 2 RECT are different(deep compare)</summary>
            public static bool operator !=(RECT rect1, RECT rect2)
            {
                return !(rect1 == rect2);
            }


        }

        [DllImport("user32")]
        internal static extern bool GetMonitorInfo(IntPtr hMonitor, MONITORINFO lpmi);

        [DllImport("user32.dll")]
        static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("User32")]
        internal static extern IntPtr MonitorFromWindow(IntPtr handle, int flags);


        #endregion
        #endregion








        private void Slider_MarketViewTextValue(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // ... Get Slider reference.
            var slider = sender as Slider;
            // ... Get Value.
            double value = slider.Value;
            // ... Set Window Title.
            TextElement.SetFontSize(DataGridStocks, value);
        }

        private void ChartButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Hey");
        }

        private void OpenChart_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Hey Button");
        }

        private void Slider_NewsViewTextValue(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // ... Get Slider reference.
            var slider = sender as Slider;
            // ... Get Value.
            double value = slider.Value;
            // ... Set Window Title.
            TextElement.SetFontSize(DataGridNews, value);
        }

        private void Slider_TwitterViewTextValue(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // ... Get Slider reference.
            var slider = sender as Slider;
            // ... Get Value.
            double value = slider.Value;
            // ... Set Window Title.
            TextElement.SetFontSize(DataGridTwitter, value);
        }

        private void Slider_StockHaltsTextValue(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // ... Get Slider reference.
            var slider = sender as Slider;
            // ... Get Value.
            double value = slider.Value;
            // ... Set Window Title.
            TextElement.SetFontSize(DataGridStockHalts, value);
        }


        async Task LoadStocksAsync()
        {
            List<Scripts.StocksTableHelper> stocksAll = new List<Scripts.StocksTableHelper>();
            List<string> stocksScanList = new List<string>();
            Random random = new Random();
            try
            {
                stocksScanList = File.ReadLines("EasyScanner.txt").ToList();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            int cnt = 0;
            foreach (var line in stocksScanList)
            {

                int randomColor = random.Next(0, 3);


                string[] lineSplit = line.Split(',');
                string randomColorString = "White";
                try
                {
                    if (lineSplit[9].Contains('-'))
                    {
                        randomColorString = "#FFD80404";
                    }
                    else if (lineSplit[9].Contains('+'))
                    {
                        randomColorString = "LawnGreen";
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }


                try
                {
                    Scripts.StocksTableHelper stocksTable = new Scripts.StocksTableHelper()
                    {
                        Time = lineSplit[1],
                        NewsCount = lineSplit[2],
                        TweetCount = Convert.ToString(random.Next(1, 25)),
                        Symbol = lineSplit[3],
                        CompanyName = lineSplit[4],
                        Bid = lineSplit[5],
                        Ask = lineSplit[6],
                        Last = lineSplit[7],
                        NetChange = lineSplit[8],
                        PercentChg = lineSplit[9],
                        Low = lineSplit[10],
                        High = lineSplit[11],
                        Volume = lineSplit[12],
                        DayOpen = lineSplit[13],
                        Trades = lineSplit[14],
                        VWAP = lineSplit[15],
                        MarketCap = lineSplit[16],
                        Float = lineSplit[17],
                        SMA20 = lineSplit[18],
                        DollarVolume = lineSplit[19],
                        Color = randomColorString
                    };
                    stocksAll.Add(stocksTable);

                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex.Message} - {cnt}");
                }

                cnt++;
            }
            foreach (var item in stocksAll)
            {

                DataGridStocks.Items.Add(item);

                // App.Current.Resources["BlueBrush"] = new SolidColorBrush(Colors.Pink);

            }



        }

        async Task LoadTweetsAsync()
        {
            List<Scripts.StocksTableHelper> tweetsAll = new List<Scripts.StocksTableHelper>();
            List<string> tweetsScanList = new List<string>();

            Random random = new Random();
            try
            {
                tweetsScanList = File.ReadLines("TweetScanner.txt").ToList();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            int cnt = 0;
            foreach (var line in tweetsScanList)
            {

                int randomColor = random.Next(0, 3);


                string[] lineSplit = line.Split(',');
                string randomColorString = "White";
                try
                {
                    if (lineSplit[9].Contains('-'))
                    {
                        randomColorString = "Red";
                    }
                    else if (lineSplit[9].Contains('+'))
                    {
                        randomColorString = "LawnGreen";
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }


                try
                {
                    Scripts.StocksTableHelper stocksTable = new Scripts.StocksTableHelper()
                    {
                        Time = lineSplit[1],
                        NewsCount = lineSplit[2],
                        Tweet = lineSplit[20],
                        Symbol = lineSplit[3],
                        CompanyName = lineSplit[4],
                        Bid = lineSplit[5],
                        Ask = lineSplit[6],
                        Last = lineSplit[7],
                        NetChange = lineSplit[8],
                        PercentChg = lineSplit[9],
                        Low = lineSplit[10],
                        High = lineSplit[11],
                        Volume = lineSplit[12],
                        DayOpen = lineSplit[13],
                        Trades = lineSplit[14],
                        VWAP = lineSplit[15],
                        MarketCap = lineSplit[16],
                        Float = lineSplit[17],
                        SMA20 = lineSplit[18],
                        DollarVolume = lineSplit[19],
                        Color = randomColorString,
                        TweetLink = "Tweet"
                    };
                    tweetsAll.Add(stocksTable);

                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex.Message} - {cnt}");
                }

                cnt++;
            }
            foreach (var item in tweetsAll)
            {

                DataGridTwitter.Items.Add(item);

                // App.Current.Resources["BlueBrush"] = new SolidColorBrush(Colors.Pink);

            }



        }

        async Task LoadNewsAsync()
        {
            List<Scripts.StocksTableHelper> newsAll = new List<Scripts.StocksTableHelper>();
            List<string> newsScanList = new List<string>();

            Random random = new Random();
            try
            {
                newsScanList = File.ReadLines("EasyScannerNewsV2.txt").ToList();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            int cnt = 0;
            foreach (var line in newsScanList)
            {

                int randomColor = random.Next(0, 3);


                string[] lineSplit = line.Split(';');
                string randomColorString = "White";
                try
                {
                    if (lineSplit[9].Contains('-'))
                    {
                        randomColorString = "Red";
                    }
                    else if (lineSplit[9].Contains('+'))
                    {
                        randomColorString = "LawnGreen";
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }


                try
                {
                    Scripts.StocksTableHelper stocksTable = new Scripts.StocksTableHelper()
                    {
                        Time = lineSplit[1],
                        News = lineSplit[20],
                        TweetCount = Convert.ToString(random.Next(1, 25)),
                        Symbol = lineSplit[3],
                        CompanyName = lineSplit[4],
                        Bid = lineSplit[5],
                        Ask = lineSplit[6],
                        Last = lineSplit[7],
                        NetChange = lineSplit[8],
                        PercentChg = lineSplit[9],
                        Low = lineSplit[10],
                        High = lineSplit[11],
                        Volume = lineSplit[12],
                        DayOpen = lineSplit[13],
                        Trades = lineSplit[14],
                        VWAP = lineSplit[15],
                        MarketCap = lineSplit[16],
                        Float = lineSplit[17],
                        SMA20 = lineSplit[18],
                        DollarVolume = lineSplit[19],
                        Color = randomColorString,
                        NewsLink = lineSplit[22]
                    };
                    newsAll.Add(stocksTable);

                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex.Message} - {cnt}");
                }

                cnt++;
            }
            foreach (var item in newsAll)
            {

                DataGridNews.Items.Add(item);

                // App.Current.Resources["BlueBrush"] = new SolidColorBrush(Colors.Pink);

            }


        }

        async Task LoadStocksHaltsAsync()
        {
            List<Scripts.StocksTableHelper> stocksHaltsAll = new List<Scripts.StocksTableHelper>();
            List<string> stocksHaltsScanList = new List<string>();
            Random random = new Random();
            try
            {
                stocksHaltsScanList = File.ReadLines("StockHalts.txt").ToList();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            int cnt = 0;
            foreach (var line in stocksHaltsScanList)
            {

                int randomColor = random.Next(0, 3);


                string[] lineSplit = line.Split(',');
                string randomColorString = "White";
                try
                {
                    if (lineSplit[9].Contains('-'))
                    {
                        randomColorString = "#FFD80404";
                    }
                    else if (lineSplit[9].Contains('+'))
                    {
                        randomColorString = "LawnGreen";
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }


                try
                {
                    Scripts.StocksTableHelper stocksTable = new Scripts.StocksTableHelper()
                    {
                        Time = lineSplit[1],
                        NewsCount = lineSplit[2],
                        TweetCount = Convert.ToString(random.Next(1, 25)),
                        Symbol = lineSplit[3],
                        CompanyName = lineSplit[4],
                        Bid = lineSplit[5],
                        Ask = lineSplit[6],
                        Last = lineSplit[7],
                        NetChange = lineSplit[8],
                        PercentChg = lineSplit[9],
                        Low = lineSplit[10],
                        High = lineSplit[11],
                        Volume = lineSplit[12],
                        DayOpen = lineSplit[13],
                        Trades = lineSplit[14],
                        VWAP = lineSplit[15],
                        MarketCap = lineSplit[16],
                        Float = lineSplit[17],
                        SMA20 = lineSplit[18],
                        DollarVolume = lineSplit[19],
                        Color = randomColorString
                    };
                    stocksHaltsAll.Add(stocksTable);

                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex.Message} - {cnt}");
                }

                cnt++;
            }
            foreach (var item in stocksHaltsAll)
            {

                DataGridStockHalts.Items.Add(item);

                // App.Current.Resources["BlueBrush"] = new SolidColorBrush(Colors.Pink);

            }



        }

        private void NewsLoadListButton_Click(object sender, RoutedEventArgs e)
        {
            List<String> newsKeywordList = new List<String>();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                newsKeywordList = File.ReadLines(openFileDialog.FileName).ToList();

            NewsKeyWordsRichTextBox.Document.Blocks.Clear();
            foreach (var item in newsKeywordList)
            {
                Paragraph paragraph = new Paragraph();
                NewsKeyWordsFlowDoc.Blocks.Add(paragraph);
                paragraph.Inlines.Add($"{item}");
            }
        }

        private void TwitterLoadListButton_Click(object sender, RoutedEventArgs e)
        {
            List<String> twitterKeywordList = new List<String>();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                twitterKeywordList = File.ReadLines(openFileDialog.FileName).ToList();

            TwitterKeyWordsRichTextBox.Document.Blocks.Clear();
            foreach (var item in twitterKeywordList)
            {
                Paragraph paragraph = new Paragraph();
                TwitterKeyWordsFlowDoc.Blocks.Add(paragraph);
                paragraph.Inlines.Add($"{item}");
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Something happened");
            List<CheckBox> checkBoxes = new List<CheckBox>();
            checkBoxes.Add(Hide1);

            Console.WriteLine(checkBoxes[0].IsChecked);

        }

        private void OpenChat_Click(object sender, RoutedEventArgs e)
        {
            Windows.Chat newChatWindow = new Windows.Chat();
            newChatWindow.Show();
        }
    }
}
