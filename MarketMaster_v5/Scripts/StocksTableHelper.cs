﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarketMaster_v5.Scripts
{
    public class StocksTableHelper
    {
        public string Time { get; set; }
        public string Price { get; set; }
        public string Size { get; set; }

        public string NewsTime { get; set; }
        public string NewsLink { get; set; }
        public string News { get; set; }

        public string TweetTime { get; set; }
        public string TweetLink { get; set; }
        public string Tweet { get; set; }

        public string NewsCount { get; set; }
        public string TweetCount { get; set; }
        public string Symbol { get; set; }
        public string CompanyName { get; set; }
        public string Bid { get; set; }
        public string Ask { get; set; }
        public string Last { get; set; }
        public string NetChange { get; set; }
        public string PercentChg { get; set; }
        public string Low { get; set; }
        public string High { get; set; }
        public string Volume { get; set; }
        public string DayOpen { get; set; }
        public string Trades { get; set; }
        public string VWAP { get; set; }
        public string MarketCap { get; set; }
        public string Float { get; set; }
        public string SMA20 { get; set; }
        public string DollarVolume { get; set; }
        public string Color { get; set; }



    }
}
