﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WinInterop = System.Windows.Interop;


namespace MarketMaster_v5.Windows
{
    /// <summary>
    /// Interaction logic for Chat.xaml
    /// </summary>
    public partial class Chat : Window
    {
        List<string> chatBoxList = new List<string>();
        List<string> usersList = new List<string>();
        public Chat()
        {
            InitializeComponent();
            winMain.SourceInitialized += new EventHandler(win_SourceInitialized);
            #region Create Users List

            usersList.Add($"BadBayMoney");
            usersList.Add($"RealTalk");
            usersList.Add($"MeetMeintheM0rning");
            usersList.Add($"KittensnSmitten");
            usersList.Add($"JustBecause");
            usersList.Add($"MyWayOrHighway");
            usersList.Add($"Eyes4Casj");
            usersList.Add($"LikeIForgotAgain");
            usersList.Add($"AlwaysNum1");
            usersList.Add($"GalaxyGroove");
            usersList.Add($"TooMuchFreeTime");
            usersList.Add($"Little_Cash");
            usersList.Add($"MusclesR");
            usersList.Add($"Fit4Days");
            usersList.Add($"EachDayIRage");
            usersList.Add($"CuzI'm___LikeThat");
            usersList.Add($"AlwaysBusyCall");
            usersList.Add($"ISeeItIWantIt");
            usersList.Add($"SierraTreking");
            usersList.Add($"FindMeHiking");
            usersList.Add($"GreenFlashSailor_");
            usersList.Add($"UndertheBorealis");
            usersList.Add($"AdMan_Money");
            usersList.Add($"FindingtheHorizon_");
            usersList.Add($"ColdWater");
            usersList.Add($"Summit2SeasExplorer");
            usersList.Add($"PacknPackOut");
            usersList.Add($"WhiteOut");
            usersList.Add($"IntotheVoid");
            usersList.Add($"HorizonHunting");
            usersList.Add($"MoreTreesPlease");
            usersList.Add($"ShowerInTheRiver");
            usersList.Add($"LeaveNoTrace");
            usersList.Add($"CampFire&Brews");
            usersList.Add($"AlpineLakesXSkyscrapes");
            usersList.Add($"Glacier&Moose");
            usersList.Add($"CinnamonBear");
            usersList.Add($"YoYosemitePacker");
            usersList.Add($"DirtBag");
            usersList.Add($"PacNWVagabond");
            usersList.Add($"EveryPeakHunter");
            usersList.Add($"Reels&RiverGirl");
            usersList.Add($"MyVanIsMyLife");
            usersList.Add($"FindgMyWay2Nowhere");
            usersList.Add($"SeeYouNever");
            usersList.Add($"StarsforNightlights");
            usersList.Add($"GoodbyeCityHello");
            usersList.Add($"RoadRamblin");
            usersList.Add($"NoCellNoProblem");
            usersList.Add($"NoServiceNoProblem");
            usersList.Add($"Campfire&Fireflies");
            usersList.Add($"WolfSongRambler");
            usersList.Add($"RoadBikes&Sweat");
            usersList.Add($"Stop&SmelltheMoney");
            #endregion

            #region Add online users

            //OnlineBox
            OnlineBox.AppendText($"BadBayMoney{Environment.NewLine}");
            OnlineBox.AppendText($"RealTalk{Environment.NewLine}");
            OnlineBox.AppendText($"MeetMeintheM0rning{Environment.NewLine}");
            OnlineBox.AppendText($"KittensnSmitten{Environment.NewLine}");
            OnlineBox.AppendText($"JustBecause{Environment.NewLine}");
            OnlineBox.AppendText($"MyWayOrHighway{Environment.NewLine}");
            OnlineBox.AppendText($"Eyes4Casj{Environment.NewLine}");
            OnlineBox.AppendText($"LikeIForgotAgain{Environment.NewLine}");
            OnlineBox.AppendText($"AlwaysNum1{Environment.NewLine}");
            OnlineBox.AppendText($"GalaxyGroove{Environment.NewLine}");
            OnlineBox.AppendText($"TooMuchFreeTime{Environment.NewLine}");
            OnlineBox.AppendText($"Little_Cash{Environment.NewLine}");
            OnlineBox.AppendText($"MusclesR{Environment.NewLine}");
            OnlineBox.AppendText($"Fit4Days{Environment.NewLine}");
            OnlineBox.AppendText($"EachDayIRage{Environment.NewLine}");
            OnlineBox.AppendText($"CuzI'm___LikeThat{Environment.NewLine}");
            OnlineBox.AppendText($"AlwaysBusyCall{Environment.NewLine}");
            OnlineBox.AppendText($"ISeeItIWantIt{Environment.NewLine}");
            OnlineBox.AppendText($"SierraTreking{Environment.NewLine}");
            OnlineBox.AppendText($"FindMeHiking{Environment.NewLine}");
            OnlineBox.AppendText($"GreenFlashSailor_{Environment.NewLine}");
            OnlineBox.AppendText($"UndertheBorealis{Environment.NewLine}");
            OnlineBox.AppendText($"AdMan_Money{Environment.NewLine}");
            OnlineBox.AppendText($"FindingtheHorizon_{Environment.NewLine}");
            OnlineBox.AppendText($"ColdWater{Environment.NewLine}");
            OnlineBox.AppendText($"Summit2SeasExplorer{Environment.NewLine}");
            OnlineBox.AppendText($"PacknPackOut{Environment.NewLine}");
            OnlineBox.AppendText($"WhiteOut{Environment.NewLine}");
            OnlineBox.AppendText($"IntotheVoid{Environment.NewLine}");
            OnlineBox.AppendText($"HorizonHunting{Environment.NewLine}");
            OnlineBox.AppendText($"MoreTreesPlease{Environment.NewLine}");
            OnlineBox.AppendText($"ShowerInTheRiver{Environment.NewLine}");
            OnlineBox.AppendText($"LeaveNoTrace{Environment.NewLine}");
            OnlineBox.AppendText($"CampFire&Brews{Environment.NewLine}");
            OnlineBox.AppendText($"AlpineLakesXSkyscrapes{Environment.NewLine}");
            OnlineBox.AppendText($"Glacier&Moose{Environment.NewLine}");
            OnlineBox.AppendText($"CinnamonBear{Environment.NewLine}");
            OnlineBox.AppendText($"YoYosemitePacker{Environment.NewLine}");
            OnlineBox.AppendText($"DirtBag{Environment.NewLine}");
            OnlineBox.AppendText($"PacNWVagabond{Environment.NewLine}");
            OnlineBox.AppendText($"EveryPeakHunter{Environment.NewLine}");
            OnlineBox.AppendText($"Reels&RiverGirl{Environment.NewLine}");
            OnlineBox.AppendText($"MyVanIsMyLife{Environment.NewLine}");
            OnlineBox.AppendText($"FindgMyWay2Nowhere{Environment.NewLine}");
            OnlineBox.AppendText($"SeeYouNever{Environment.NewLine}");
            OnlineBox.AppendText($"StarsforNightlights{Environment.NewLine}");
            OnlineBox.AppendText($"GoodbyeCityHello{Environment.NewLine}");
            OnlineBox.AppendText($"RoadRamblin{Environment.NewLine}");
            OnlineBox.AppendText($"NoCellNoProblem{Environment.NewLine}");
            OnlineBox.AppendText($"NoServiceNoProblem{Environment.NewLine}");
            OnlineBox.AppendText($"Campfire&Fireflies{Environment.NewLine}");
            OnlineBox.AppendText($"WolfSongRambler{Environment.NewLine}");
            OnlineBox.AppendText($"RoadBikes&Sweat{Environment.NewLine}");
            OnlineBox.AppendText($"Stop&SmelltheMoney{Environment.NewLine}");
            #endregion

            #region Add lines to chatbox

            chatBoxList.Add("Watch it is up and down be careful");
            chatBoxList.Add("S-3 from skyline Medical shows Registration for $20M Mixed Securities Shelf Offering… What does this mean?");
            chatBoxList.Add("Hi guys any thoughts on $AMD, I'm thinking of buying");
            chatBoxList.Add("My Monday morning W/L - $CLDX — based on charts and recent news -  meaning no bad news");
            chatBoxList.Add("wow! $CLDX looks promising");
            chatBoxList.Add("Check out $AEHR its on multi month techical breakout");
            chatBoxList.Add("Finally finished all 14 parts of Trader Checklist, would have finished sooner if I didn’t rewatch them");
            chatBoxList.Add("$BLRX one of my top watches tomorrow. Actually happy its fixing to be Monday");
            chatBoxList.Add("Anyone shorting $TKAI");
            chatBoxList.Add("What’s the new hot patterns trending in this market?");
            chatBoxList.Add("Been watching $TKAI made 20 bucks");
            chatBoxList.Add("$OPEC meeting this Wednesday. Hopefully the oil optimism continues");
            chatBoxList.Add("Watch $BLRX Monday has legs I missed it was out of town");
            chatBoxList.Add("Wanted to get in at 1.13 but missed it");
            chatBoxList.Add("$BLRX looks like it’ll be a good short this week. Can still run up though");
            chatBoxList.Add("Actually $BLRX looks like it can form my fav b/o pattern if it b/o past 1.42. the b/o pattered hasn’t been working lately though. Idk if I trust it");
            chatBoxList.Add("Little_Cash joined the chat");
            chatBoxList.Add("Anyone watching $BLRX");
            #endregion
            LoadAlertData();
            PoulateChatBox();
        }


        #region Windows Controls
        private void MinimizeButton_Click(object sender, RoutedEventArgs ex)
        {

            this.WindowState = WindowState.Minimized;

        }

        private void MaximizeButton_Click(object sender, RoutedEventArgs ex)
        {
            if (this.WindowState == WindowState.Maximized)
            {
                this.WindowState = WindowState.Normal;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs ex)
        {
            this.Close();
        }





        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //UserControl usc = null;
            //GridMain.Children.Clear();

            //switch (((ListViewItem)((ListView)sender).SelectedItem).Name)
            //{
            //    case "ItemHome":
            //        usc = new UserControlHome();
            //        GridMain.Children.Add(usc);
            //        break;
            //    case "ItemCreate":
            //        usc = new UserControlCreate();
            //        GridMain.Children.Add(usc);
            //        break;
            //    default:
            //        break;
            //}
        }


        #region Avoid hiding task bar upon maximalisation

        private static System.IntPtr WindowProc(
              System.IntPtr hwnd,
              int msg,
              System.IntPtr wParam,
              System.IntPtr lParam,
              ref bool handled)
        {
            switch (msg)
            {
                case 0x0024:
                    WmGetMinMaxInfo(hwnd, lParam);
                    handled = false;
                    break;
            }

            return (System.IntPtr)0;
        }

        void win_SourceInitialized(object sender, EventArgs e)
        {
            System.IntPtr handle = (new WinInterop.WindowInteropHelper(this)).Handle;
            WinInterop.HwndSource.FromHwnd(handle).AddHook(new WinInterop.HwndSourceHook(WindowProc));
        }

        private static void WmGetMinMaxInfo(System.IntPtr hwnd, System.IntPtr lParam)
        {

            MINMAXINFO mmi = (MINMAXINFO)Marshal.PtrToStructure(lParam, typeof(MINMAXINFO));

            // Adjust the maximized size and position to fit the work area of the correct monitor
            int MONITOR_DEFAULTTONEAREST = 0x00000002;
            System.IntPtr monitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);

            if (monitor != System.IntPtr.Zero)
            {

                MONITORINFO monitorInfo = new MONITORINFO();
                GetMonitorInfo(monitor, monitorInfo);
                RECT rcWorkArea = monitorInfo.rcWork;
                RECT rcMonitorArea = monitorInfo.rcMonitor;
                mmi.ptMaxPosition.x = Math.Abs(rcWorkArea.left - rcMonitorArea.left);
                mmi.ptMaxPosition.y = Math.Abs(rcWorkArea.top - rcMonitorArea.top);
                mmi.ptMaxSize.x = Math.Abs(rcWorkArea.right - rcWorkArea.left);
                mmi.ptMaxSize.y = Math.Abs(rcWorkArea.bottom - rcWorkArea.top);
                mmi.ptMaxTrackSize.x = Math.Abs(rcWorkArea.Width);
                mmi.ptMaxTrackSize.y = Math.Abs(rcWorkArea.Height);
            }

            Marshal.StructureToPtr(mmi, lParam, true);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            /// <summary>
            /// x coordinate of point.
            /// </summary>
            public int x;
            /// <summary>
            /// y coordinate of point.
            /// </summary>
            public int y;

            /// <summary>
            /// Construct a point of coordinates (x,y).
            /// </summary>
            public POINT(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MINMAXINFO
        {
            public POINT ptReserved;
            public POINT ptMaxSize;
            public POINT ptMaxPosition;
            public POINT ptMinTrackSize;
            public POINT ptMaxTrackSize;
        };

        void win_Loaded(object sender, RoutedEventArgs e)
        {
            winMain.WindowState = WindowState.Maximized;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class MONITORINFO
        {
            /// <summary>
            /// </summary>            
            public int cbSize = Marshal.SizeOf(typeof(MONITORINFO));

            /// <summary>
            /// </summary>            
            public RECT rcMonitor = new RECT();

            /// <summary>
            /// </summary>            
            public RECT rcWork = new RECT();

            /// <summary>
            /// </summary>            
            public int dwFlags = 0;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 0)]
        public struct RECT
        {
            /// <summary> Win32 </summary>
            public int left;
            /// <summary> Win32 </summary>
            public int top;
            /// <summary> Win32 </summary>
            public int right;
            /// <summary> Win32 </summary>
            public int bottom;

            /// <summary> Win32 </summary>
            public static readonly RECT Empty = new RECT();

            /// <summary> Win32 </summary>
            public int Width
            {
                get { return Math.Abs(right - left); }  // Abs needed for BIDI OS
            }
            /// <summary> Win32 </summary>
            public int Height
            {
                get { return bottom - top; }
            }

            /// <summary> Win32 </summary>
            public RECT(int left, int top, int right, int bottom)
            {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }


            /// <summary> Win32 </summary>
            public RECT(RECT rcSrc)
            {
                this.left = rcSrc.left;
                this.top = rcSrc.top;
                this.right = rcSrc.right;
                this.bottom = rcSrc.bottom;
            }

            /// <summary> Win32 </summary>
            public bool IsEmpty
            {
                get
                {
                    // BUGBUG : On Bidi OS (hebrew arabic) left > right
                    return left >= right || top >= bottom;
                }
            }
            /// <summary> Return a user friendly representation of this struct </summary>
            public override string ToString()
            {
                if (this == RECT.Empty) { return "RECT {Empty}"; }
                return "RECT { left : " + left + " / top : " + top + " / right : " + right + " / bottom : " + bottom + " }";
            }

            /// <summary> Determine if 2 RECT are equal (deep compare) </summary>
            public override bool Equals(object obj)
            {
                if (!(obj is Rect)) { return false; }
                return (this == (RECT)obj);
            }

            /// <summary>Return the HashCode for this struct (not garanteed to be unique)</summary>
            public override int GetHashCode()
            {
                return left.GetHashCode() + top.GetHashCode() + right.GetHashCode() + bottom.GetHashCode();
            }


            /// <summary> Determine if 2 RECT are equal (deep compare)</summary>
            public static bool operator ==(RECT rect1, RECT rect2)
            {
                return (rect1.left == rect2.left && rect1.top == rect2.top && rect1.right == rect2.right && rect1.bottom == rect2.bottom);
            }

            /// <summary> Determine if 2 RECT are different(deep compare)</summary>
            public static bool operator !=(RECT rect1, RECT rect2)
            {
                return !(rect1 == rect2);
            }


        }

        [DllImport("user32")]
        internal static extern bool GetMonitorInfo(IntPtr hMonitor, MONITORINFO lpmi);

        [DllImport("user32.dll")]
        static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("User32")]
        internal static extern IntPtr MonitorFromWindow(IntPtr handle, int flags);


        #endregion
        #endregion

        private void SendDayButton_Click(object sender, RoutedEventArgs e)
        {
            AddToRichTextBox();
        }

        protected void link_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Link Clicked");
            //var hyperlink = (Hyperlink)sender;
            //Console.WriteLine(hyperlink);
        }

        public void AddToRichTextBox()
        {
            string currentTime = $"{DateTime.Now.ToString("HH:mm:ss")}";

            if (ClientBox.Text.Contains("$AAPL"))
            {

                ChangeSymbolToLink("$AAPL", ClientBox.Text);
            }
            else if (ClientBox.Text.Contains("$AMD"))
            {
                ChangeSymbolToLink("$AMD", ClientBox.Text);
            }
            else if (ClientBox.Text.Contains("$CLDX"))
            {
                ChangeSymbolToLink("$CLDX", ClientBox.Text);
            }
            else if (ClientBox.Text.Contains("$WYY"))
            {
                ChangeSymbolToLink("$WYY", ClientBox.Text);
            }
            else if (ClientBox.Text.Contains("$RGSE"))
            {
                ChangeSymbolToLink("$RGSE", ClientBox.Text);
            }
            else if (ClientBox.Text.Contains("$BLRX"))
            {
                ChangeSymbolToLink("$BLRX", ClientBox.Text);
            }
            else
            {
                Paragraph paragraph = new Paragraph();
                myFlowDoc.Blocks.Add(paragraph);
                paragraph.Inlines.Add($"{currentTime} - {ClientBox.Text}");
            }

            ServerBox.ScrollToEnd();

            ClientBox.Clear();
        }

        private void OnlineBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Random random = new Random();
            OnlineBox.Selection.ApplyPropertyValue(RichTextBox.ForegroundProperty, Brushes.Red);
            if (random.Next(1, 4) == 2)
            {
                OnlineBox.Selection.ApplyPropertyValue(RichTextBox.ForegroundProperty, Brushes.Red);
            }
            else if (random.Next(1, 4) == 3)
            {
                OnlineBox.Selection.ApplyPropertyValue(RichTextBox.ForegroundProperty, Brushes.Green);
            }
            else
            {
                OnlineBox.Selection.ApplyPropertyValue(RichTextBox.ForegroundProperty, Brushes.White);
            }

        }

        public void LoadAlertData()
        {
            Scripts.StocksTableHelper stocksTable = new Scripts.StocksTableHelper()
            {
                Symbol = $"$JMU",
                Last = $"1.49",
                PercentChg = $"+10.87",
                Color = "LawnGreen"
            };
            DataGridXMAL.Items.Insert(0, stocksTable);
            stocksTable = new Scripts.StocksTableHelper()
            {
                Symbol = $"$AMD",
                Last = $"42.85",
                PercentChg = $"+3.15",
                Color = "LawnGreen"
            };
            DataGridXMAL.Items.Insert(0, stocksTable);
            stocksTable = new Scripts.StocksTableHelper()
            {
                Symbol = $"$TMSR",
                Last = $"2.38",
                PercentChg = $"+8.29",
                Color = "LawnGreen"
            };
            DataGridXMAL.Items.Insert(0, stocksTable);
        }

        public void PoulateChatBox()
        {

            string currentTime = $"{DateTime.Now.ToString("HH:mm:ss")}";

            for (int i = 0; i < 18; i++)
            {
                string clientBox = chatBoxList[i];
                Console.WriteLine($"{i} - {clientBox}");


                if (clientBox.Contains("$AAPL"))
                {

                    ChangeSymbolToLink("$AAPL", clientBox);
                }
                else if (clientBox.Contains("$AMD"))
                {
                    ChangeSymbolToLink("$AMD", clientBox);
                }
                else if (clientBox.Contains("$CLDX"))
                {
                    ChangeSymbolToLink("$CLDX", clientBox);
                }
                else if (clientBox.Contains("$WYY"))
                {
                    ChangeSymbolToLink("$WYY", clientBox);
                }
                else if (clientBox.Contains("$RGSE"))
                {
                    ChangeSymbolToLink("$RGSE", clientBox);
                }
                else if (clientBox.Contains("$BLRX"))
                {
                    ChangeSymbolToLink("$BLRX", clientBox);
                }
                else if (clientBox.Contains("$TKAI"))
                {
                    ChangeSymbolToLink("$TKAI", clientBox);
                }
                else if (clientBox.Contains("$AEHR"))
                {
                    ChangeSymbolToLink("$AEHR", clientBox);
                }
                else if (clientBox.Contains("$OPEC"))
                {
                    ChangeSymbolToLink("$OPEC", clientBox);
                }
                else
                {
                    Paragraph paragraph = new Paragraph();
                    myFlowDoc.Blocks.Add(paragraph);
                    paragraph.Inlines.Add($"{currentTime} - {chatBoxList[i]}");
                }
                ServerBox.ScrollToEnd();
            }


        }



        public void ChangeSymbolToLink(string sysmbol, string clientBox)
        {
            Random rand = new Random();
            try
            {
                string currentTime = $"{DateTime.Now.ToString("HH:mm:ss")}";

                int index = clientBox.IndexOf(sysmbol);


                Hyperlink link = new Hyperlink();
                link.IsEnabled = true;
                Paragraph paragraph = new Paragraph();
                myFlowDoc.Blocks.Add(paragraph);
                link.Inlines.Add(sysmbol);
                link.Click += new RoutedEventHandler(this.link_Click);
                paragraph.Inlines.Add($"{currentTime} ");
                paragraph.Inlines.Add($"{usersList.OrderBy(x => Guid.NewGuid()).FirstOrDefault()}: ");
                paragraph.Inlines.Add($"{clientBox.Substring(0, index)} ");
                paragraph.Inlines.Add(link);
                paragraph.Inlines.Add($" {clientBox.Substring(index + 5)}");
                ServerBox.Document = myFlowDoc;
            }
            catch (Exception ex)
            {

                Console.WriteLine($"{clientBox} {ex.Message}");
            }
        }
    }
}
